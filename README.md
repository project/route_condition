This module provides a Drupal 8 Condition plugin based on route names.
Condition plugins can be used in various places, e.g. to determine block 
visibility, as a condition for the Context module, etc...

This plugin is similar to the Drupal 8 core RequestPath ("Pages")
plugin, but allows you to match on route names (e.g. "user.entity.canonical") 
instead of URL paths.
