/**
 * @file
 * Attaches behaviors for the Route condition module.
 */
(function ($, Drupal) {
  /**
   * Behaviors for block settings summaries.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.routeConditionSettingsSummaries = {
    attach(context) {
      // The drupalSetSummary method required for this behavior is not available
      // on the Blocks administration page, so we need to make sure this
      // behavior is processed only if drupalSetSummary is defined.
      if (typeof jQuery.fn.drupalSetSummary === 'undefined') {
        return;
      }

      $('[data-drupal-selector="edit-visibility-route"]').drupalSetSummary(
        (context) => {
          const routesElement = document.querySelector(
            'textarea[name="visibility[route][routes]"]',
          );
          const routes = routesElement && routesElement.value;
          return routes
            ? Drupal.t('Restricted to certain routes')
            : Drupal.t('Not restricted');
        },
      );
    },
  };
})(jQuery, Drupal);
